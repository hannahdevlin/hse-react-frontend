import * as actionTypes from "./shopping-types";
import axios from 'axios';

export const fetchProducts = () => {
  // const baseURL = 'http://35.233.32.52:8000';
  // const apiURL = baseURL + '/api/health_promotions/products/';

  return (dispatch) => {
    dispatch(fetchProductsRequest);
    axios.get('http://35.233.32.52:8000/api/health_promotions/products/')
    .then(response => {
      const products = response.data;
      dispatch(fetchProductsSuccess(products));
    })
    .catch(error => {
      const errorMsg = error.message;
      dispatch(fetchProductsFailure(errorMsg));
    })
  }
}

export const fetchProductsRequest = () => {
  return {
    type: actionTypes.FETCH_PRODUCTS_REQUEST
  }
}

export const fetchProductsSuccess = products => {
  return {
    type: actionTypes.FETCH_PRODUCTS_SUCCESS,
    payload: products
  }
}

export const fetchProductsFailure = error => {
  return {
    type: actionTypes.FETCH_PRODUCTS_FAIL,
    payload: error
  }
}

export const addToCart = (itemID) => {
  return {
    type: actionTypes.ADD_TO_CART,
    payload: {
      id: itemID,
    },
  };
};

export const removeFromCart = (itemID) => {
  return {
    type: actionTypes.REMOVE_FROM_CART,
    payload: {
      id: itemID,
    },
  };
};

export const adjustItemQty = (itemID, qty) => {
  return {
    type: actionTypes.ADJUST_ITEM_QTY,
    payload: {
      id: itemID,
      qty,
    },
  };
};

export const loadCurrentItem = (item) => {
  return {
    type: actionTypes.LOAD_CURRENT_ITEM,
    payload: item,
  };
};
