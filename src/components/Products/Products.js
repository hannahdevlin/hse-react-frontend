import React, { useEffect } from "react";
import styles from "./Products.module.css";
import Product from "./Product/Product";

// Redux
import { connect } from "react-redux";
import { fetchProducts } from "../../redux/Shopping/shopping-actions";

function Products ({ products, fetchProducts }) {

  useEffect(() => {
    fetchProducts();
  }, [])

  return (
    <div className={styles.products}>
      {products.map((product) => (
        <Product key={product.id} product={product} />
      ))}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    products: state.shop.products,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchProducts: () => dispatch(fetchProducts())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);
